import random

# Prepare a list
numbers = []
# For a hundred times...
for number in range(101):
    # add a random number between 1 and 1000
    numbers.append(random.randint(1, 1000))

# Print the largest number to output
print("The largest number is ", max(numbers))
