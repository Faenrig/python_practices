import random

# Prepare a list
numbers = []
# For a hundred times...
for number in range(101):
    # Add a random number between 1 and 1000 to the list
    numbers.append(random.randint(1, 1000))

# Get the average value of the list and cut from the second decimal point
formatted_number = "{:.2f}".format(sum(numbers) / len(numbers))
# Print the average number to the output
print("The average of this run is ", formatted_number)
