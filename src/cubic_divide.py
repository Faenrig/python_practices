# For every number from 1000 to 1...
for number in range(1000, 0, -1):
    # Get the number powered by three
    pow_number = pow(number, 3)
    # If the number is dividable by five
    if pow_number % 5 == 0:
        # Print the number
        print(pow_number)
