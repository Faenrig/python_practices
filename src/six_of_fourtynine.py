import random

# Mimics a lottery pull with 6 numbers from 1 to 49
def six_of_fourtynine():
    # Prepare a list
    numbers = []
    # For six times...
    for number in range(7):
        # Add a random number between 1 and 49 to the list
        numbers.append(random.randint(1, 49))
        
    # Print the list of numbers
    print(numbers)

six_of_fourtynine()
